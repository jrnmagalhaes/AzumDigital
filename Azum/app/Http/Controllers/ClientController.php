<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DateTime;

class ClientController extends Controller
{
    function registerClient(Request $request){
        $clientID = DB::table('clients')->insertGetId([
            'name' => $request->nome . ' ' . $request->sobrenome,
            'email' => $request->email,
            'password' => 'mudar123',
            'phone_number' => $request->telefone,
            'cpfcnpj' => $request->cpfcnpj,
            'created_at' => new DateTime(),
            'updated_at' => new DateTime()
        ]);
        $productID = DB::table('products')->insertGetId([
            'criacao_arte' => $request->criacao_arte,
            'planejamento_acoes' => $request->planejamento_acoes,
            'publicacoes_social' => $request->publicacoes_social,
            'aumento_seguidores' => $request->aumento_seguidores,
            'consultoria_postagem' => $request->consultoria_postagem,
            'interacao_fans' => $request->interacao_fans,
            'imagens_para_texto' => $request->imagens_para_texto,
            'responsivo_instagram' => $request->responsivo_instagram,
            'direct_leads' => $request->direct_leads
        ]);
        DB::table('client_product')
            ->insert([
                [
                    'client_id' => $clientID, 
                    'product_id' => $productID,
                    'created_at' => new DateTime(),
                    'updated_at' => new DateTime()
                ]
            ]);

        return redirect('/');
    }

    function choicePlan(Request $request){
        $tosend = [];
        
        if ( $request->criacao_arte == null ) {
            $tosend['criacao_arte'] = 0;
        } else {
            $tosend['criacao_arte'] = $request->criacao_arte;
        }

        if ( isset($request->planejamento_acoes) ) {
            if ( $request->planejamento_acoes == "on" ) {
                $tosend['planejamento_acoes'] = 1;
            } else {
                $tosend['planejamento_acoes'] = $request->planejamento_acoes;
            }
        } else {
            $tosend['planejamento_acoes'] = 0;
        }

        if ( isset($request->publicacoes_social) ) {
            if ( $request->publicacoes_social == "on" ) {
                $tosend['publicacoes_social'] = 1;
            } else {
                $tosend['publicacoes_social'] = $request->publicacoes_social;
            }
        } else {
            $tosend['publicacoes_social'] = 0;
        }

        if ( isset($request->aumento_seguidores) ) {
            if ( $request->aumento_seguidores == "on" ) {
                $tosend['aumento_seguidores'] = 1;
            } else {
                $tosend['aumento_seguidores'] = $request->aumento_seguidores;
            }
        } else {
            $tosend['aumento_seguidores'] = 0;
        }

        if ( isset($request->consultoria_postagem) ) {
            if ( $request->consultoria_postagem == "on" ) {
                $tosend['consultoria_postagem'] = 1;
            } else {
                $tosend['consultoria_postagem'] = $request->consultoria_postagem;
            }
        } else {
            $tosend['consultoria_postagem'] = 0;
        }

        if ( isset($request->interacao_fans) ) {
            if ( $request->interacao_fans == "on" ) {
                $tosend['interacao_fans'] = 1;
            } else {
                $tosend['interacao_fans'] = $request->interacao_fans;
            }
        } else {
            $tosend['interacao_fans'] = 0;
        }

        if ( isset($request->imagens_para_texto) ) {
            if ( $request->imagens_para_texto == "on" ) {
                $tosend['imagens_para_texto'] = 1;
            } else {
                $tosend['imagens_para_texto'] = $request->imagens_para_texto;
            }
        } else {
            $tosend['imagens_para_texto'] = 0;
        }

        if ( isset($request->responsivo_instagram) ) {
            if ( $request->responsivo_instagram == "on" ) {
                $tosend['responsivo_instagram'] = 1;
            } else {
                $tosend['responsivo_instagram'] = $request->responsivo_instagram;
            }
        } else {
            $tosend['responsivo_instagram'] = 0;
        }

        if ( isset($request->direct_leads) ) {
            if ( $request->direct_leads == "on" ) {
                $tosend['direct_leads'] = 1;
            } else {
                $tosend['direct_leads'] = $request->direct_leads;
            }
        } else {
            $tosend['direct_leads'] = 0;
        }

        // dd($tosend);
        return redirect('cadastro')
                ->with('criacao_arte', $tosend['criacao_arte'])
                ->with('planejamento_acoes', $tosend['planejamento_acoes'])
                ->with('publicacoes_social', $tosend['publicacoes_social'])
                ->with('aumento_seguidores', $tosend['aumento_seguidores'])
                ->with('consultoria_postagem', $tosend['consultoria_postagem'])
                ->with('interacao_fans', $tosend['interacao_fans'])
                ->with('imagens_para_texto', $tosend['imagens_para_texto'])
                ->with('responsivo_instagram', $tosend['responsivo_instagram'])
                ->with('direct_leads', $tosend['direct_leads']);
    }
}
