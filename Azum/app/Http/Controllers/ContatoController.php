<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContatoMail;
use App\Mail\VendaMail;
use Illuminate\Support\Facades\Mail;

class ContatoController extends Controller
{
    function enviarContato(Request $request){
        $email = [];
        $email['nome'] = $request->nome;
        $email['empresa'] = $request->empresa;
        $email['email'] = $request->email;
        $email['segmento'] = $request->segmento;
        if ( isset($request->pergunta) ) {
            $email['pergunta'] = $request->pergunta;
        }
        $email['mensagem'] = $request->mensagem;
        $email['opcoes'] = [];
        if ( isset($request->desejada) ) {
            array_push($email['opcoes'], 'Fazer com que minha empresa seja desejada');
        }
        if ( isset($request->clientes) ) {
            array_push($email['opcoes'], 'Encontrar clientes que tenham potencial de compra');
        }
        if ( isset($request->vendas) ) {
            array_push($email['opcoes'], 'Fazer o fechamento da venda');
        }
        if ( isset($request->experiencia) ) {
            array_push($email['opcoes'], 'Oferecer a melhor experiência para nossa base de clientes');
        }
        Mail::send(new ContatoMail($email));
        return redirect('/contato');
    }

    function enviarVendas(Request $request){
        $email = [];
        $email['nome'] = $request->nome;
        $email['email'] = $request->email;
        $email['telefone'] = $request->telefone;
        $email['mensagem'] = $request->mensagem;
        Mail::send(new VendaMail($email));
        return redirect()->back();
    }
}
