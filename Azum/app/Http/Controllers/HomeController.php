<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = DB::table('clients')
            ->select(
                'clients.id',
                'clients.name',
                'clients.phone_number',
                'clients.email',
                'products.criacao_arte', 
                'products.aumento_seguidores', 
                'products.gestao_anuncios', 
                'products.consultoria_postagem', 
                'products.publicacoes_social', 
                'products.imagens_para_texto', 
                'products.responsivo_instagram', 
                'products.interacao_fans', 
                'products.direct_leads'
            )
            ->leftJoin(
                'client_product',
                'client_id', '=', 'clients.id'
            )
            ->leftJoin(
                'products',
                'products.id', '=', 'client_product.product_id'
            )
            ->orderBy('clients.id', 'DESC')
            ->paginate(10);
        return view('home', ["clients" => $clients]);
    }
}
