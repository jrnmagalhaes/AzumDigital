<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    public function clients() {
        return $this->belongsToMany('App\Client', 'client_product', 'product_id', 'client_id')->withTimestamps();
    }
}
