<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['phone_number', 'name', 'email', 'password', 'cpfcnpj'];

    public function products() {
        return $this->belongsToMany('App\Product', 'client_product', 'client_id', 'product_id')->withTimestamps();
    }

}
