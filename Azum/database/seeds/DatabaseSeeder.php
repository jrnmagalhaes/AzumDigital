<?php

use Illuminate\Database\Seeder;
use App\Client;
use App\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Rodolfo',
            'email' => 'rodolfo@azum.digital',
            'password' => bcrypt('password')
        ]);
        for($i = 0; $i < 10; $i++){
            DB::table('clients')->insert([
                'name' => str_random(10),
                'phone_number' => '(75) 992636567',
                'email' => str_random(10).'@gmail.com',
                'password' => bcrypt('secret'),
                'created_at' => now()->format('Y-m-d H:i:s')
                ]);
        }
        for($i = 0; $i < 10; $i++){
            DB::table('products')->insert([
                'criacao_arte' => random_int ( 0 , 100 ), 
                'aumento_seguidores' => random_int ( 0 , 1 ), 
                'gestao_anuncios' => random_int ( 0 , 1 ), 
                'consultoria_postagem' => random_int ( 0 , 1 ), 
                'publicacoes_social' => random_int ( 0 , 1 ), 
                'imagens_para_texto' => random_int ( 0 , 100 ), 
                'responsivo_instagram' => random_int ( 0 , 1 ), 
                'interacao_fans' => random_int ( 0 , 1 ), 
                'direct_leads' => random_int ( 0 , 1 )
                ]);
        }
        foreach(Client::all() as $client){
            $client->products()->save(Product::find($client->id), ['created_at' => now()->format('Y-m-d H:i:s')]);
        }
    }
}
