<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('criacao_arte');
            $table->boolean('planejamento_acoes');
            $table->boolean('publicacoes_social');
            $table->boolean('aumento_seguidores');
            $table->boolean('consultoria_postagem');
            $table->boolean('interacao_fans');
            $table->integer('imagens_para_texto');
            $table->boolean('responsivo_instagram');
            $table->boolean('direct_leads');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
