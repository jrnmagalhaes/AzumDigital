<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/depoimentos', function () {
    return view('depoimentos');
});
Route::get('/gerenciamento', function () {
    return view('gerenciamento');
});
Route::get('/leads', function () {
    return view('leads');
});
Route::get('/contato', function () {
    return view('contato');
});
Route::get('/sobre-nos', function () {
    return view('sobrenos');
});
Route::get('/cadastro', function () {
    return view('registro');
});

Route::post('/enviar-contato', 'ContatoController@enviarContato');
Route::post('/enviar-vendas', 'ContatoController@enviarVendas');

// Auth::routes();
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');


// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

/**
 * Cadastro de clientes pelo site
 */
Route::post('/register-client', 'ClientController@registerClient');
Route::post('/choice-plan', 'ClientController@choicePlan');
