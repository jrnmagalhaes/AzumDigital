@extends('base')

@section('content')
    <section class="topo" data-page="depoimentos">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro">
                <h1>O que eles falam sobre nós</h1>
                <button class="button">Conheça</button>
            </div>
        </div>
    </section>

    <section class="william-ux-designer">
        <div class="container">
            <div class="linha-flex wrap william-ux-designer-container">
                <iframe class="depoimentos-iframe" src="https://www.youtube.com/embed/nlcIKh6sBtc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                <div class="flex-1 william-ux-designer-content">
                    <div class="text-align-center">
                        <img class="william-ux-designer-image" src="./images/william-safadao.png" alt="">
                    </div>
                    <img class="chat-image margin-top-50" src="./images/chat-outline.svg" alt="">
                    <p>Com o Gerenciamento de redes sociais, o nosso crescimento foi de 32% nos últimos 8 meses. Mas o mais impressionante não é o número em si, e sim o fato disso ter acontecido depois de 2 anos de estagnação.</p>
                    <h4>WILLIAM GOMES, ESPECIALISTA EM UX DA AZUN DIGITAL</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="depoimentos-section animation-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Resultados reais e aqui <br> compartilhados</h2>
                    <small>Veja mais!</small>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="depoimento-container text-align-center">
                        <img class="icon" src="./images/depoiment-icon.png" alt="">
                        <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quaerat deleniti optio explicabo iusto cumque. Voluptates, debitis placeat provident architecto aliquam aperiam? Pariatur praesentium, libero ut adipisci odio placeat recusandae ullam.</p>
                        <img class="client" src="./images/placeholder-cliente.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div id="scene">
            <div data-depth="0.2">
                <img src="./images/gerencimanto-bg/youtube.png" alt="">
            </div>
            <div data-depth="0.4">
                <img src="./images/gerencimanto-bg/baloon.png" alt="">
            </div>
            <div data-depth="0.6">
                <img src="./images/gerencimanto-bg/youtube2.png" alt="">
            </div>
        </div>
    </section>

    @component('components.contrate-gerenciamento')
        @slot('color')
            cinza
        @endslot
    @endcomponent

    @component('components.vendas-formulario')
        @slot('color')
            branco
        @endslot
    @endcomponent 

    <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
    <script>
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);
    </script>
@endsection