<section data-color={{ $color }} class="vendas-formulario-container">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-align-center">
                <h2>Sua empresa <br> 
                    precisa de, vendas ou experiência/sucesso <br> dos clientes?
                </h2>
            </div>
        </div>
        <form class="margin-top-10" action="/enviar-vendas" method="POST">
            @csrf
            <div class="row">
                <div class="col-xs-12">
                    <input required type="text" name="nome" placeholder="Seu nome...">
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <input required type="email" name="email" placeholder="Seu email...">
                </div>
                <div class="col-sm-6">
                    <input required type="phone" name="telefone" class="telefone" placeholder="Seu telefone...">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <textarea required name="mensagem" placeholder="Sua mensagem..." rows="5"></textarea>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-align-center margin-top-10">
                    <button class="button large" type="submit">Enviar</button>
                </div>
            </div>
        </form>
    </div>
</section>