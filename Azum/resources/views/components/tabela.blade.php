<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-align-center">
                <h2>Monte o plano ideal para sua <br>
                    empresa</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 pricing-table-container">
                <div class="fixed-colum">
                    <div class="indexes">
                        <ul class="list-unstyled">
                            <li>Criação de arte para rede social instagram e facebook </li>
                            <li>Planejamento de ações</li>
                            <li>Postagens na rede social facebook instagram</li>
                            <li>Sistema de aumento de seguidores reais</li>
                            <li>Consultoria de postagens para rede social</li>
                            <li>Interação com fãs</li>
                            <li>Imagens profissionais para textos</li>
                            <li>Layout responsivo instagram</li>
                            <li>Estratégia DirectLeads</li>
                        </ul>
                    </div>
                </div>
                <div class="overflowed-columns">
                    <div class="price-colum">
                        <form action="/choice-plan" method="POST">
                            @csrf
                            <div class="column-header esquerda">
                                <h4>Bronze</h4>
                                <img src="./images/aviao-basico.png" alt="">
                            </div>
                            <div class="column-pricing">
                                <div>
                                    <p class="moeda">R$</p>
                                    <p class="preco">250</p>
                                </div>
                                <p class="periodo">/mês</p>
                            </div>
                            <div class="column-especs">
                                <ul class="list-unstyled">
                                    <li>
                                        8/mês
                                        <input type="hidden" name="criacao_arte" value="8">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="planejamento_acoes" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="publicacoes_social" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="aumento_seguidores" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="consultoria_postagem" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="interacao_fans" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="imagens_para_texto" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="responsivo_instagram" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="direct_leads" value="0">
                                    </li>
                                </ul>
                            </div>
                            <div class="column-footer esquerda text-align-center">
                                <button type="submit" class="button contratar">Contratar</button>
                            </div>
                        </form>
                    </div>
                    <div class="price-colum">
                        <form action="/choice-plan" method="POST">
                            @csrf
                            <div class="column-header">
                                <h4>Prata</h4>
                                <img src="./images/aviao-medio.png" alt="">
                            </div>
                            <div class="column-pricing">
                                <div>
                                    <p class="moeda">R$</p>
                                    <p class="preco">400</p>
                                </div>
                                <p class="periodo">/mês</p>
                            </div>
                            <div class="column-especs">
                                <ul class="list-unstyled">
                                    <li>
                                        12/mês
                                        <input type="hidden" name="criacao_arte" value="12">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="planejamento_acoes" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="publicacoes_social" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="aumento_seguidores" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="consultoria_postagem" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="interacao_fans" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="imagens_para_texto" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="responsivo_instagram" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="direct_leads" value="0">
                                    </li>
                                </ul>
                            </div>
                            <div class="column-footer text-align-center">
                                <button type="submit" class="button contratar">Contratar</button>
                            </div>
                        </form>
                    </div>
                    <div class="price-colum">
                        <form action="/choice-plan" method="POST">
                            @csrf
                            <div class="column-header">
                                <h4>Ouro</h4>
                                <img src="./images/aviao-maximo.png" alt="">
                            </div>
                            <div class="column-pricing">
                                <div>
                                    <p class="moeda">R$</p>
                                    <p class="preco">750</p>
                                </div>
                                <p class="periodo">/mês</p>
                            </div>
                            <div class="column-especs">
                                <ul class="list-unstyled">
                                    <li>
                                        20/mês
                                        <input type="hidden" name="criacao_arte" value="20">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="planejamento_acoes" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="publicacoes_social" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="aumento_seguidores" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="consultoria_postagem" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/visto.png" alt="">
                                        <input type="hidden" name="interacao_fans" value="1">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="imagens_para_texto" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="responsivo_instagram" value="0">
                                    </li>
                                    <li>
                                        <img src="./images/naovisto.png" alt="">
                                        <input type="hidden" name="direct_leads" value="0">
                                    </li>
                                </ul>
                            </div>
                            <div class="column-footer text-align-center">
                                <button class="button contratar">Contratar</button>
                            </div>
                        </form>
                    </div>
                    <div class="price-colum">
                        <form action="/choice-plan" method="POST">
                            @csrf
                            <div class="column-header direita">
                                <h4>Monte o seu</h4>
                            </div>
                            <div class="column-pricing direita">
                                <div>
                                    <p class="moeda">R$</p>
                                    <p id="preco-monte-o-seu" valor="0" class="preco">0</p>
                                </div>
                                <p class="periodo">/mês</p>
                            </div>
                            <div class="column-especs direita">
                                <ul class="list-unstyled">
                                    <li>
                                        <input id="criacao_arte" type="number" name="criacao_arte" class="table-input tocalc">
                                    </li>
                                    <li>
                                        <input id="planejamento_acoes" name="planejamento_acoes" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="publicacoes_social" name="publicacoes_social" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="aumento_seguidores" name="aumento_seguidores" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="consultoria_postagem" name="consultoria_postagem" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="interacao_fans" name="interacao_fans" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="imagens_para_texto" name="imagens_para_texto" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="responsivo_instagram" name="responsivo_instagram" type="checkbox" class="tocalc">
                                    </li>
                                    <li>
                                        <input id="direct_leads" name="direct_leads" type="checkbox" class="tocalc">
                                    </li>
                                </ul>
                            </div>
                            <div class="column-footer direita text-align-center">
                                <button class="button contratar">Contratar</button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<script>
    
</script>