@extends('base')

@section('content')
    <section class="topo" data-page="sobrenos">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro">
                <h1>Lorem ipsum dolum sit amet, <br> consectetur adipiscing elit</h1>
                <button class="button">Conheça</button>
            </div>
        </div>
    </section>

    <section class="about-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="about-content">
                        <h2>lorem ipsum dolor sit</h2>
                        <p>
                            A necessidade que aconteceu da empresa familiar precisar de um marketing digital porem não existia nenhuma agencia que suprisse o que era necessário, a partir disso o CEO começou a estudar sobre marketing para aplicar na empresa familiar e se apaixonou pela área. Após o trabalho realizado na empresa familiar ter dado certo o CEO percebeu que trabalhar na empresa da família não era o que ele procurava e sim ajudar as empresas a se destacarem digitalmente, inseri-las no mundo digital era o prazer que ele havia escoberto. Desde então ele começou a estudar e se aprofundar mais na área e abriu o que hoje se chama Azun. Com a idei a de que todos precisam estar inseridos no mundo digital trabalhamos para que você não apenas apareça lá e sim se destaque entre os milhares que existe.
                        </p>
                        <div class="about-bagde">
                            <img src="./images/azum-idea.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="long-exposure-section">

            <img src="./images/long-exposure.png" alt="">
            <div class="long-esposure-content">
                <h2>lorem ipsum dolum sit <br> amet, consectur</h2>
            </div>
        </div>
    </section>


    <section class="about-us-video">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>lorem ipsum dolum sit amet</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <iframe class="full-page-iframe" src="https://www.youtube.com/embed/nlcIKh6sBtc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

    @component('components.contrate-gerenciamento')
        @slot('color')
            cinza
        @endslot
    @endcomponent

    @component('components.vendas-formulario')
        @slot('color')
            branco
        @endslot
    @endcomponent
@endsection