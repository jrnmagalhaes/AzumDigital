@extends('base')

@section('content')
    <section class="topo" data-page="home">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro">
                <h1>O serviço que pode mudar <br> tudo</h1>
                <button class="button">Conheça</button>
            </div>
        </div>
    </section>


    <section class="especialistas-container animation-container">
        <div class="container animation-content">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Especialistas em Gerenciamento <br>
                            de Redes Sociais</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <iframe class="full-page-iframe" src="https://www.youtube.com/embed/nlcIKh6sBtc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div id="scene">
            <div data-depth="0.2">
                <img src="./images/gerencimanto-bg/youtube.png" alt="">
            </div>
            <div data-depth="0.4">
                <img src="./images/gerencimanto-bg/baloon.png" alt="">
            </div>
            <div data-depth="0.6">
                <img src="./images/gerencimanto-bg/youtube2.png" alt="">
            </div>
        </div>
    </section>

    <section class="servicos-section">
        <div class="container">
            <div class="row text-align-center">
                <div class="col-xs-12">
                    <h2>Serviços</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>01</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>02</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>03</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>04</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>05</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="servico linha-flex start">
                        <h3>06</h3>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nec placerat velit. Nam eu accumsan quam, ac interdum nunc. Orci varius natoque penatibus et magnis dis parturient montes.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="call-to-action-section">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro text-align-center">
                <img class="chat-outline-1" src="./images/chat-outline.svg" alt="">
                <h2>Lorem ipsum dolor sit amet, consectetur <br> adipiscing elit. Aliquam tempus tellus at <br> varius interdum.</h2>
                <button class="button large">Nossos planos</button>
            </div>
        </div>
    </section>

    @component('components.tabela')
    @endcomponent

    <section class="slider-section margin-top-50">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Resultados de nossos clientes. <br> Cases de sucesso</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="main-carousel">
                        <div class="carousel-cell">
                            <img src="./images/placeholder-cliente.png" alt="">
                            <p>
                                Com o Gerenciamento de redes sociais, o nosso crescimento foi de 32% nos últimos 8 meses. Mas o mais impressionante não é o número em si, e sim o fato disso ter acontecido depois de 2 anos de estagnação.
                            </p>
                            <h3>Junior Marcarenhas, CEO da ABSAM</h3>
                        </div>
                        <div class="carousel-cell">
                            <img src="./images/placeholder-cliente.png" alt="">
                            <p>
                                Com o Gerenciamento de redes sociais, o nosso crescimento foi de 32% nos últimos 8 meses. Mas o mais impressionante não é o número em si, e sim o fato disso ter acontecido depois de 2 anos de estagnação.
                            </p>
                            <h3>Junior Marcarenhas, CEO da ABSAM</h3>
                        </div>
                        <div class="carousel-cell">
                            <img src="./images/placeholder-cliente.png" alt="">
                            <p>
                                Com o Gerenciamento de redes sociais, o nosso crescimento foi de 32% nos últimos 8 meses. Mas o mais impressionante não é o número em si, e sim o fato disso ter acontecido depois de 2 anos de estagnação.
                            </p>
                            <h3>Junior Marcarenhas, CEO da ABSAM</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @component('components.contrate-gerenciamento')
        @slot('color')
            cinza
        @endslot
    @endcomponent

    @component('components.vendas-formulario')
        @slot('color')
            branco
        @endslot
    @endcomponent

    <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
    <script>
        var scene = document.getElementById('scene');
        var parallaxInstance = new Parallax(scene);

        
    </script>
@endsection
    