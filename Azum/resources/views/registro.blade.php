@extends('base-auth')

@section('content')
    <h1>Cadastro</h1>
    <form action="/register-client" method="POST">
        @csrf
        <input type="hidden" name="criacao_arte" value={{session('criacao_arte')}}>
        <input type="hidden" name="planejamento_acoes" value={{session('planejamento_acoes')}}>
        <input type="hidden" name="publicacoes_social" value={{session('publicacoes_social')}}>
        <input type="hidden" name="aumento_seguidores" value={{session('aumento_seguidores')}}>
        <input type="hidden" name="consultoria_postagem" value={{session('consultoria_postagem')}}>
        <input type="hidden" name="interacao_fans" value={{session('interacao_fans')}}>
        <input type="hidden" name="imagens_para_texto" value={{session('imagens_para_texto')}}>
        <input type="hidden" name="responsivo_instagram" value={{session('responsivo_instagram')}}>
        <input type="hidden" name="direct_leads" value={{session('direct_leads')}}>
        <input required type="text" name="nome" placeholder="Nome">
        <input required type="text" name="sobrenome" placeholder="Sobrenome">
        <input required type="email" name="email" placeholder="Email">
        <input required type="text" name="cpfcnpj" placeholder="CPF ou CNPJ">
        <input required type="phone" name="telefone" class="telefone" placeholder="Telefone">
        <button type="submit" class="button large">Cadastrar</button>
    </form>
@endsection