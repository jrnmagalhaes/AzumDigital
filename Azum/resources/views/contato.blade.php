@extends('base')

@section('content')
    <section class="topo" data-page="contato">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro">
                <h1>Lorem ipsum dolum sit amet, <br> consectetur adipiscing elit</h1>
                <button class="button">Conheça</button>
            </div>
        </div>
    </section>


    <section class="formulario-contato-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Tem alguma dúvida?</h2>
                </div>
            </div>
            <form action="/enviar-contato" method="POST">
                @csrf
                <div class="row">
                    <div class="col-xs-12">
                        <input type="text" required name="nome" placeholder="Seu nome*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="text" required name="empresa" placeholder="Sua empresa*">
                    </div>
                    <div class="col-sm-6">
                        <input type="text" required name="segmento" placeholder="Qual segmento?*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <input type="email" required name="email" placeholder="E-mail*">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <select name="pergunta">
                            <option disabled selected>Perguntas frequentes...</option>
                            <option value="teste">teste</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 margin-top-10">
                        <label class="checkbox-container"> Fazer com que minha empresa seja desejada
                            <input name="desejada" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-xs-12">
                        <label class="checkbox-container"> Encontrar clientes que tenham potencial de compra
                            <input name="clientes" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-xs-12">
                        <label class="checkbox-container"> Fazer o fechamento da venda
                            <input name="vendas" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-xs-12">
                        <label class="checkbox-container"> Oferecer a melhor experiência para nossa base
                                de clientes
                            <input name="experiencia" type="checkbox">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 text-align-center">
                        <h2>Há algo mais que queira <br>
                                nos dizer?</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <textarea name="mensagem" rows="6" placeholder="Mensagem..."></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 margin-top-10 text-align-center">
                        <button class="button large" type="submit">Enviar</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection