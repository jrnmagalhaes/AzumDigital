<h1>{{$email['nome']}} - {{$email['empresa']}}</h1>
<h3>{{$email['segmento']}}</h3>
<h2>{{$email['email']}}</h2>
<br>
<br>
<hr>
@if ( isset($email['pergunta']) )
    <h3>Pergunta: {{$email['pergunta']}}</h3>
    <hr>
    <br>
    <br>
@endif
@if ( sizeof($email['opcoes']) > 0 )
<ul>
    @foreach ($email['opcoes'] as $opcao)
        <li>
            {{$opcao}}
        </li>
    @endforeach
</ul>
<br>
<br>
<hr>
@endif
<p>{{$email['mensagem']}}</p>
