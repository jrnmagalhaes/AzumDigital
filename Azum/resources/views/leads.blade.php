@extends('base')

@section('content')
    <section class="topo" data-page="leads">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro text-align-center">
                <h3>CAPTAMOS LEADS PARA SEU NEGÓCIO</h3>
                <h1>Aumente suas vendas em até 49% sem sair de casa!</h1>
                <h3>JÁ SABE O QUE SÃO LEADS?</h3>
                <button class="button contratar margin-top-10">Sim, quero contratar!</button>
            </div>
        </div>
    </section>

    <section class="wtf-are-leads-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Geração de <b>leads</b></h2>
                </div>
            </div>
            <div class="row linha-flex alinha-centro margin-top-50 wrap">
                <div class="col-sm-7 col-xs-12 col-xs-12 wow bounceIn" data-wow-delay="0.5s" data-wow-offset="300">
                    <div class="explanation-card linha-flex">
                        <div class="image-container coluna-flex justificar-centro">
                            <img src="./images/chat.svg" alt="">
                        </div>
                        <div>
                            <h3>Por que Leads são tão importantes?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a consequat felis. Vivamus aliquet nisi id neque elementum, eget tincidunt diam lacinia. Aenean ultricies neque sed lacus feugiat, sit amet aliquam nisi hendrerit. Etiam ultrices, mi ut posuere dictum, risus purus pellentesque ex.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 explanation-image text-align-center wow slideInRight" data-wow-offset="300">
                    <img class="engine" src="./images/engine.svg" alt="">
                </div>
            </div>
            <div class="row linha-flex alinha-centro margin-top-50 wrap">
                <div class="col-sm-5 col-xs-12 explanation-image text-align-center wow slideInLeft" data-wow-offset="300">
                    <img src="./images/robo.svg" alt="">
                </div>
                <div class="col-sm-7 col-xs-12 wow bounceIn" data-wow-delay="0.5s" data-wow-offset="300">
                    <div class="explanation-card linha-flex">
                        <div class="image-container coluna-flex justificar-centro">
                            <img src="./images/chat.svg" alt="">
                        </div>
                        <div>
                            <h3>Como esses Leads são captados?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a consequat felis. Vivamus aliquet nisi id neque elementum, eget tincidunt diam lacinia. Aenean ultricies neque sed lacus feugiat, sit amet aliquam nisi hendrerit. Etiam ultrices, mi ut posuere dictum, risus purus pellentesque ex.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row linha-flex alinha-centro margin-top-50 wrap">
                <div class="col-sm-7 col-xs-12 wow bounceIn" data-wow-delay="0.5s" data-wow-offset="300">
                    <div class="explanation-card linha-flex">
                        <div class="image-container coluna-flex justificar-centro">
                            <img src="./images/chat.svg" alt="">
                        </div>
                        <div>
                            <h3>Por que Leads são tão importantes?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a consequat felis. Vivamus aliquet nisi id neque elementum, eget tincidunt diam lacinia. Aenean ultricies neque sed lacus feugiat, sit amet aliquam nisi hendrerit. Etiam ultrices, mi ut posuere dictum, risus purus pellentesque ex.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-5 col-xs-12 explanation-image text-align-center wow slideInRight" data-wow-offset="300">
                    <img src="./images/chart.svg" alt="">
                </div>
            </div>
            <div class="row linha-flex alinha-centro margin-top-50 wrap">
                <div class="col-sm-5 col-xs-12 explanation-image text-align-center wow slideInLeft" data-wow-offset="300">
                    <img src="./images/infografico.svg" alt="">
                </div>
                <div class="col-sm-7 col-xs-12 wow bounceIn" data-wow-delay="0.5s" data-wow-offset="300">
                    <div class="explanation-card linha-flex">
                        <div class="image-container coluna-flex justificar-centro">
                            <img src="./images/chat.svg" alt="">
                        </div>
                        <div>
                            <h3>Como esses Leads são captados?</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam a consequat felis. Vivamus aliquet nisi id neque elementum, eget tincidunt diam lacinia. Aenean ultricies neque sed lacus feugiat, sit amet aliquam nisi hendrerit. Etiam ultrices, mi ut posuere dictum, risus purus pellentesque ex.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    @component('components.contrate-gerenciamento')
        @slot('color')
            cinza
        @endslot
    @endcomponent

    <section>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>Aprovado por nossos <br> clientes</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <iframe class="full-page-iframe" src="https://www.youtube.com/embed/nlcIKh6sBtc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

    @component('components.vendas-formulario')
        @slot('color')
            branco
        @endslot
    @endcomponent
@endsection