@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Clientes</div>
                <div class="card-body">
                    <table class="table table-responsive">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Nome</th>
                                <th>Telefone</th>
                                <th>E-mail</th>
                                <th>Criação<br> de Arte</th>
                                <th>Aumento <br> Seguidores</th>
                                <th>Gestão <br> Anuncios</th>
                                <th>Consultoria<br> Postagem</th>
                                <th>Publicações<br> Redes Sociais</th>
                                <th>Imagens <br>para textos</th>
                                <th>Responsivo <br>instagram</th>
                                <th>Interação <br>Fãns</th>
                                <th>Direct <br>Leads</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($clients as $client)
                            <tr>
                                <td>{{ $client->id }}</td>
                                <td>{{ $client->name }}</td>
                                <td>{{ $client->phone_number }}</td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->criacao_arte }}</td>
                                <td>{{ $client->aumento_seguidores == 1? "true" : "false" }}</td>
                                <td>{{ $client->gestao_anuncios == 1? "true" : "false" }}</td>
                                <td>{{ $client->consultoria_postagem == 1? "true" : "false" }}</td>
                                <td>{{ $client->publicacoes_social == 1? "true" : "false" }}</td>
                                <td>{{ $client->imagens_para_texto}}</td>
                                <td>{{ $client->responsivo_instagram == 1? "true" : "false" }}</td>
                                <td>{{ $client->interacao_fans == 1? "true" : "false" }}</td>
                                <td>{{ $client->direct_leads == 1? "true" : "false" }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $clients->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
