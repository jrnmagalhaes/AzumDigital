<!doctype html>
<html lang="pr-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/animate.css">
        <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
        <link rel="stylesheet" href="css/style.css">

    </head>
    <body>
        <header data-spy="affix" data-offset-top="150">
            <div class="container">
                <div class="row linha-flex alinha-centro">
                    <div class="col-sm-2 col-xs-6">
                        <img src="images/logo.png" alt="">
                    </div>
                    <div class="col-sm-10 text-align-right menu">
                        <ul class="list-inline">
                            <li class="{{ Request::path() == '/' ? 'active' : '' }}"> <a href="/"> Home</a></li>
                            <li class="{{ Request::path() == 'depoimentos' ? 'active' : '' }}"> <a href="depoimentos"> Depoimentos</a></li>
                            <li class="{{ Request::path() == 'gerenciamento' ? 'active' : '' }}"> <a href="gerenciamento"> Gerenciamento</a></li>
                            <li class="{{ Request::path() == 'leads' ? 'active' : '' }}"> <a href="leads"> Leads</a></li>
                            <li class="{{ Request::path() == 'sobre-nos' ? 'active' : '' }}"> <a href="sobre-nos"> Sobre nós</a></li>
                            <li class="{{ Request::path() == 'contato' ? 'active' : '' }}"> <a href="contato"> contato</a></li>
                            <li> <a class="button" href="login"> Painel</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>

        @yield('content')

        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <img class="footer-logo" src="images/logo-branca.png" alt="">
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li><a href="">Lorem ipsum</a></li>
                            <li><a href="">Lorem ipsum</a></li>
                            <li><a href="">Lorem ipsum</a></li>
                        </ul>
                        <ul class="list-inline social">
                            <li><a href=""><img src="images/instagram.svg" alt=""></a></li>
                            <li><a href=""><img src="images/facebook.svg" alt=""></a></li>
                            <li><a href=""><img src="images/youtube.svg" alt=""></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3">
                        <ul class="list-unstyled">
                            <li><a href="">Lorem ipsum</a></li>
                            <li><a href="">Lorem ipsum</a></li>
                            <li><a href="">Lorem ipsum</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 last-line">
                        <p>Todos os direitos reservados. Azun digital </p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Latest compiled and minified JavaScript -->
        <script
			  src="https://code.jquery.com/jquery-3.3.1.min.js"
			  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
              crossorigin="anonymous"></script>
        <script src="./js/menuchef.js"></script>
        <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="./js/wow.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.js"></script>
        <script>
            new WOW().init();
            jQuery("input.telefone")
                .mask("(99) 9999-9999?9")
                .focusout(function (event) {  
                    var target, phone, element;  
                    target = (event.currentTarget) ? event.currentTarget : event.srcElement;  
                    phone = target.value.replace(/\D/g, '');
                    element = $(target);  
                    element.unmask();  
                    if(phone.length > 10) {  
                        element.mask("(99) 99999-999?9");  
                    } else {  
                        element.mask("(99) 9999-9999?9");  
                    }  
                });
            if ( $(window).outerWidth() < 567 ) {
                new MenuChef('.menu ul>li>a', {
                    theme: {
                        theme: 'side',
                        effectOnOpen: 'smooth'
                    }
                });
                $('.menu').remove();


                $('.main-carousel').flickity({
                    // options
                    cellAlign: 'center',
                    contain: true,
                    wrapAround: true,
                    prevNextButtons: false,
                    autoPlay: true
                });
            } else {
                $('.main-carousel').flickity({
                    // options
                    cellAlign: 'center',
                    contain: true,
                    pageDots: false,
                    wrapAround: true
                });
            }

            $( document ).ready(function() {
                calcularValor();
            });

            function calcularValor(){
                var total = $("#criacao_arte").val() * 25;
                if ( $("#planejamento_acoes").prop('checked') ) {
                    total += 30
                }
                if ( $("#publicacoes_social").prop('checked') ) {
                    total += 20
                }
                if ( $("#aumento_seguidores").prop('checked') ) {
                    total += 50
                }
                if ( $("#consultoria_postagem").prop('checked') ) {
                    total += 100
                }
                if ( $("#interacao_fans").prop('checked') ) {
                    total += 50
                }
                if ( $("#imagens_para_texto").prop('checked') ) {
                    total += 10
                }
                if ( $("#responsivo_instagram").prop('checked') ) {
                    total += 100
                }
                if ( $("#direct_leads").prop('checked') ) {
                    total += 200
                }
                console.log(total)
                $("#preco-monte-o-seu").html(total)
            }


            //tabela de preços

            $('.tocalc').on('input',function(e){

                //modificações no input de quantidade de artes criadas
                if ( $(this).attr("id") == "criacao_arte" ){
                    if ( $(this).val() == 0 || $(this).val() == "" ) {
                        $("#publicacoes_social").prop('checked', false);
                        $("#responsivo_instagram").prop('checked', false);
                    }
                }

                //caso em que o input de postagem em rede social foi clicado
                if ( $(this).attr("id") == "publicacoes_social" ){
                    if ( this.checked ) {
                        let criacao_value = $("#criacao_arte").val();
                        if ( criacao_value == "" || criacao_value == 0) {
                            $("#criacao_arte").val(1)
                        }
                    }
                }

                //caso em que o input de postagem em rede social foi clicado
                if ( $(this).attr("id") == "responsivo_instagram" ){
                    if ( this.checked ) {
                        let criacao_value = $("#criacao_arte").val();
                        if ( criacao_value == "" || criacao_value == 0) {
                            $("#criacao_arte").val(1)
                        }
                    }
                }
                
                calcularValor()
            });



        </script>
    </body>
</html>
