@extends('base')

@section('content')
    <section class="topo" data-page="gerenciamento">
        <div class="container">
            <div class="coluna-flex justificar-centro alinha-centro">
                <h1>Lorem ipsum dolum sit amet, <br> consectetur adipiscing elit</h1>
                <button class="button">Conheça</button>
            </div>
        </div>
    </section>

    <section class="marketers-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <h2>
                        Lorem ipsum dolor sit amet consectetur adipisicing
                    </h2>
                    <p class="margin-top-50">
                        Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ea saepe, accusantium distinctio facilis voluptates dolor inventore alias placeat atque fuga, voluptatum sint illo obcaecati suscipit. Quis, hic fugiat! Quas, laboriosam.
                    </p>
                    <h3>
                        WILLIAM GOMES, ESPECIALISTA EM UX DA AZUN DIGITAL
                    </h3>
                </div>
                <div class="col-sm-5">
                    <img src="./images/marketers-conversando.png" alt="">
                </div>
            </div>
            <div class="row margin-top-50">
                <div class="col-xs-12 text-align-center">
                    <iframe class="full-page-iframe" src="https://www.youtube.com/embed/nlcIKh6sBtc?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </section>

    @component('components.tabela')
    @endcomponent

    <section class="clientes-marcas-container">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 text-align-center">
                    <h2>
                    Lorem ipsum dolor sit amet, <br> consectetur adipiscing elit.  
                    </h2>
                </div>
            </div>
            <div class="row margin-top-50">
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
            </div>
            <div class="row margin-top-50">
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
                <div class="col-sm-4">
                    <img src="./images/logo_absam.png" alt="">
                </div>
            </div>
        </div>
    </section>

    @component('components.contrate-gerenciamento')
        @slot('color')
            branco
        @endslot
    @endcomponent

    @component('components.vendas-formulario')
        @slot('color')
            cinza
        @endslot
    @endcomponent
@endsection